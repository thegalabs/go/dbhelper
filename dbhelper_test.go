package dbhelper_test

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/getiota/go/dbhelper"
	"gorm.io/gorm"
)

type Dummy struct {
	ID        uint   `gorm:"primarykey"`
	Something string `gorm:"unique"`
}

var dbMutext sync.Mutex

func OpenTestDB(t *testing.T) *gorm.DB {
	dbMutext.Lock()

	db, err := dbhelper.Open()
	assert.Nil(t, err)
	err = db.AutoMigrate(&Dummy{})

	assert.Nil(t, err)
	assert.Nil(t, db.Exec("DELETE FROM dummies").Error)

	return db
}

func ReleaseTestDB() {
	dbMutext.Unlock()
}

func TestIsDuplicate(t *testing.T) {
	db := OpenTestDB(t)

	assert.Nil(t, db.Create(&Dummy{Something: "hello"}).Error)

	assert.True(t, dbhelper.IsDuplicateError(db.Create(&Dummy{Something: "hello"}).Error))

	ReleaseTestDB()
}
