module gitlab.com/getiota/go/dbhelper

go 1.13

require (
	github.com/GoogleCloudPlatform/cloudsql-proxy v1.18.0
	github.com/jackc/pgconn v1.7.0
	github.com/mattn/go-sqlite3 v1.14.3
	github.com/rs/zerolog v1.19.0
	github.com/stretchr/testify v1.5.1
	gorm.io/driver/postgres v1.0.5
	gorm.io/driver/sqlite v1.1.3
	gorm.io/gorm v1.20.5
)
